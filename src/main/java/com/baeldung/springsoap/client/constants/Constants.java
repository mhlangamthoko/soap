package com.baeldung.springsoap.client.constants;

public @interface Constants {
    String CONTEXT_PATH = "com.baeldung.springsoap.client.gen";
    String NAMESPACE_URI = "http://www.baeldung.com/springsoap/gen";

}
